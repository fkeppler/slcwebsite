<?php
	/*
		Export
		Western Washington University
		Service Learning Center Database
	*/
	include 'database.php';
	$database = connectSLC();
	
	//Post the file name to outname
	//File name will be 'output.csv' if not posted
	$outname = $_POST['outname'];
	if ($outname) $filename = $outname . ".csv";
	else $filename = "output.csv";
	
	//Header info
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename='.$filename);
	
	//Post the SQL query to export
	if (isset($_POST['export']))
	{
		//Query the database
		$query = $_POST['export'];
		$query = str_replace('\\', '', $query);
		$sql = mysqli_query($database, $query) or die("Error: " . mysqli_error($database));
		$columns = $sql->field_count;

		// Open php's stdout for use with fputcsv
		$outfile = fopen('php://output', 'w');

		// Write the table name so we can easily import
		echo $outname;
		echo "\n";
		
		//Write field names to the first row
		for ($i = 0; $i < $columns; $i++)
		{
			$heading = $sql->fetch_field_direct($i)->name;
			echo $heading;
			echo ",";
		}
		echo "\n";
		//Write each row
		
		if($_POST["exportKeyword"])
		{
			$rowIndex = 0;
			$exportRows = $_POST["exportRows"];
		}
		while ($row = mysqli_fetch_array($sql, MYSQL_ASSOC))
		{
			if ($_POST["exportKeyword"])
			{	
				if (in_array(strval($rowIndex), $exportRows)){
					fputcsv($outfile, $row);
				}
			}
			else
			{
				fputcsv($outfile, $row);
			}
			
			if($_POST["exportKeyword"])
			{
				$rowIndex = $rowIndex + 1;
			}
		}

		// Close file used for output
		fclose($outfile);

		//Echo the table
		echo $output;
	}
?>
