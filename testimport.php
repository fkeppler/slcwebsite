<?php
	/* Testing page for import.php.
	 * Delete after all tests have been run
 	 */
	include_once('CAS.php');
	phpCAS::client(CAS_VERSION_2_0, 'websso.wwu.edu', 443, '/cas');
	phpCAS::setNoCasServerValidation();
	if (!phpCAS::isAuthenticated()) phpCAS::forceAuthentication();
	if (isset($_REQUEST['logout'])) phpCAS::logout();
	if (phpCAS::isAuthenticated()) $casuser = phpCAS::getUser();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Testing page for import.php</title>
</head>

<body>
	<form action="import.php" method="post" enctype="multipart/form-data">
		<input type="file" name="csvfile" />
		<input type="submit" value="Send" />	
	</form>
</body>
</html>
