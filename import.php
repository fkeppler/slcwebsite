<?php
	/* Import.php
	 * Western Washington University
	 * Service Learning Center Database
	 * Imports an uploaded CSV file and enters it into the DB
	 */
	include 'database.php';
	$database = connectSLC();

	$tables_list = array(
			'affiliate', 'course', 'faculty', 'facultyComments',
			'partner', 'partnerComments', 'project', 'projectComments',
			'section', 'sectionComments', 'users'
	);

	// Check if file uploaded is valid
	if ($_FILES['csvfile']['error'] === UPLOAD_ERR_OK
		&& is_uploaded_file($_FILES['csvfile']['tmp_name'])) {

		// Get temporary file from upload
		$csvfile = fopen($_FILES['csvfile']['tmp_name'], 'r');

		// Get table name
		$table_name = fgetcsv($csvfile)[0] or die("Could not find a table name");
		if (!in_array($table_name, $tables_list)) {
			fclose($csvfile);
			die("File contained invalid table name");
		}

		// Get column names
		$column_names = fgetcsv($csvfile);
		foreach ($column_names as &$col) {
			$col = $database->real_escape_string($col);
		}
		unset($col); // Keep ourselves from repeating last element DON'T TOUCH!

		// Insert each of the rows of the csv file into the DB
		while (($row = fgetcsv($csvfile)) !== false) {
			// Add column names
			$query = 'REPLACE INTO ' . $table_name . ' (';
			foreach($column_names as $col) {
				$query .= ($col . ', ');
			}
			$query = rtrim($query, ", ");

			// Add column values
			$query .= ') VALUES (';
			foreach($row as $col) {
				$query .= ('\'' . $database->real_escape_string($col) . '\', ');
			}
			$query = rtrim($query, ", ");
			$query .= ');';

			// Execute query
			$database->fullQuery($query);
		}
		echo "File Uploaded";
	}
	else {
		die("No file uploaded");
	}

?>
