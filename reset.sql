-- MySQL dump 10.14  Distrib 5.5.33-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: slc
-- ------------------------------------------------------
-- Server version	5.5.33-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `affiliate`
--

DROP TABLE IF EXISTS `affiliate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliate` (
  `aff_ID` int(10) NOT NULL AUTO_INCREMENT,
  `aff_projectID` int(10) NOT NULL,
  `aff_sectionID` int(10) NOT NULL,
  PRIMARY KEY (`aff_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `courseID` int(10) NOT NULL AUTO_INCREMENT,
  `courseNum` varchar(50) DEFAULT NULL,
  `courseDept` varchar(50) DEFAULT NULL,
  `courseName` varchar(100) DEFAULT NULL,
  `courseDesc` varchar(5000) DEFAULT NULL,
  `courseCreator` varchar(50) DEFAULT NULL,
  `courseLastMod` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`courseID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `faculty`
--

DROP TABLE IF EXISTS `faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faculty` (
  `facultyID` int(10) NOT NULL AUTO_INCREMENT,
  `facultyName` varchar(50) DEFAULT NULL,
  `facultyDept` varchar(50) DEFAULT NULL,
  `facultyTraining` bit(1) DEFAULT NULL,
  `facultyDesc` varchar(5000) DEFAULT NULL,
  `facultyCreator` varchar(50) DEFAULT NULL,
  `facultyLastMod` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`facultyID`)
) ENGINE=InnoDB AUTO_INCREMENT=30682 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facultyComments`
--

DROP TABLE IF EXISTS `facultyComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facultyComments` (
  `facultyComID` int(10) NOT NULL AUTO_INCREMENT,
  `facultyCom` varchar(1000) NOT NULL,
  `comFK_facultyID` int(10) NOT NULL,
  `facultyComCreator` varchar(50) DEFAULT NULL,
  `facultyComLastMod` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`facultyComID`),
  KEY `comFK_facultyID_idx` (`comFK_facultyID`),
  CONSTRAINT `comFK_facultyID` FOREIGN KEY (`comFK_facultyID`) REFERENCES `faculty` (`facultyID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `partnerID` int(10) NOT NULL AUTO_INCREMENT,
  `partnerName` varchar(50) DEFAULT NULL,
  `partnerContact` varchar(100) DEFAULT NULL,
  `partnerType` varchar(50) DEFAULT NULL,
  `partnerDesc` varchar(5000) DEFAULT NULL,
  `partnerCreator` varchar(50) DEFAULT NULL,
  `partnerLastMod` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`partnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partnerComments`
--

DROP TABLE IF EXISTS `partnerComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partnerComments` (
  `partnerComID` int(10) NOT NULL AUTO_INCREMENT,
  `partnerCom` varchar(1000) NOT NULL,
  `comFK_partnerID` int(10) NOT NULL,
  `partnerComCreator` varchar(50) DEFAULT NULL,
  `partnerComLastMod` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`partnerComID`),
  KEY `comFK_partnerID_idx` (`comFK_partnerID`),
  CONSTRAINT `comFK_partnerID` FOREIGN KEY (`comFK_partnerID`) REFERENCES `partner` (`partnerID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `projectID` int(10) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(50) DEFAULT NULL,
  `projectNumStudents` int(10) DEFAULT '0',
  `projectHours` int(10) DEFAULT '0',
  `projectType` varchar(50) DEFAULT NULL,
  `projectStatus` varchar(50) DEFAULT NULL,
  `projectTimeSensitive` bit(1) DEFAULT NULL,
  `projectDesc` varchar(5000) DEFAULT NULL,
  `projectCreator` varchar(50) DEFAULT NULL,
  `projectLastMod` varchar(50) DEFAULT NULL,
  `projectFK_partnerID` int(10) DEFAULT NULL,
  PRIMARY KEY (`projectID`),
  KEY `projectFK_partnerID_idx` (`projectFK_partnerID`),
  CONSTRAINT `projectFK_partnerID` FOREIGN KEY (`projectFK_partnerID`) REFERENCES `partner` (`partnerID`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projectComments`
--

DROP TABLE IF EXISTS `projectComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectComments` (
  `projectComID` int(10) NOT NULL AUTO_INCREMENT,
  `projectCom` varchar(1000) NOT NULL,
  `comFK_projectID` int(10) NOT NULL,
  `projectComCreator` varchar(50) DEFAULT NULL,
  `projectComLastMod` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`projectComID`),
  KEY `comFK_projectID_idx` (`comFK_projectID`),
  CONSTRAINT `comFK_projectID` FOREIGN KEY (`comFK_projectID`) REFERENCES `project` (`projectID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `sectionID` int(10) NOT NULL AUTO_INCREMENT,
  `sectionCRN` varchar(50) DEFAULT NULL,
  `sectionQuarter` varchar(50) DEFAULT NULL,
  `sectionYear` varchar(50) DEFAULT NULL,
  `sectionStatus` varchar(50) DEFAULT NULL,
  `sectionNumStudents` int(10) DEFAULT '0',
  `communityEngagementHours` int(10) DEFAULT '0',
  `sectionSL` bit(1) DEFAULT NULL,
  `sectionRefl` bit(1) DEFAULT NULL,
  `sectionEval` bit(1) DEFAULT NULL,
  `sectionPres` bit(1) DEFAULT NULL,
  `sectionDesc` bit(1) DEFAULT NULL,
  `sectionCreator` varchar(50) DEFAULT NULL,
  `sectionLastMod` varchar(50) DEFAULT NULL,
  `sectionFK_courseID` int(10) DEFAULT NULL,
  `sectionFK_facultyID` int(10) DEFAULT NULL,
  PRIMARY KEY (`sectionID`),
  KEY `sectionFK_courseID_idx` (`sectionFK_courseID`),
  KEY `sectionFK_facultyID_idx` (`sectionFK_facultyID`),
  CONSTRAINT `sectionFK_courseID` FOREIGN KEY (`sectionFK_courseID`) REFERENCES `course` (`courseID`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `sectionFK_facultyID` FOREIGN KEY (`sectionFK_facultyID`) REFERENCES `faculty` (`facultyID`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sectionComments`
--

DROP TABLE IF EXISTS `sectionComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sectionComments` (
  `sectionComID` int(10) NOT NULL AUTO_INCREMENT,
  `sectionCom` varchar(1000) NOT NULL,
  `comFK_sectionID` int(10) NOT NULL,
  `sectionComCreator` varchar(50) DEFAULT NULL,
  `sectionComLastMod` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sectionComID`),
  KEY `comFK_sectionID_idx` (`comFK_sectionID`),
  CONSTRAINT `comFK_sectionID` FOREIGN KEY (`comFK_sectionID`) REFERENCES `section` (`sectionID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `privilege` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-26  0:36:51
