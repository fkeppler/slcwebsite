<?php
	/*
		Export
		Western Washington University
		Service Learning Center Database
	*/
	$filename = "backup_file.sql";
	$permissions_array = parse_ini_file("database.config");
	$username = $permissions_array["db_username"];
	$password = $permissions_array["db_password"];
	$host = $permissions_array["db_hostname"];
    $dbname = $permissions_array["database_name"];
	exec('mysqldump --user=' . $username . ' --password=' . $password . ' --host=' . $host . ' ' . 	$dbname . ' > ' . $filename);
	
	//Get file type and set it as Content Type
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    header('Content-Type: ' . finfo_file($finfo, $filename));
    finfo_close($finfo);

    //Use Content-Disposition: attachment to specify the filename
    header('Content-Disposition: attachment; filename='.basename($filename));

    //No cache
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');

    //Define file size
    header('Content-Length: ' . filesize($filename));

    ob_clean();
    flush();
    readfile($filename);
    exit;
?>
