<?php
/*
	Course Profile
	Western Washington University
	Service Learning Center Database
*/
include('database.php');
include_once('CAS.php');
phpCAS::client(CAS_VERSION_2_0, 'websso.wwu.edu', 443, '/cas');
phpCAS::setNoCasServerValidation();
if (!phpCAS::isAuthenticated()) phpCAS::forceAuthentication();
if (isset($_REQUEST['logout'])) phpCAS::logout();
if (phpCAS::isAuthenticated()) $casuser = phpCAS::getUser();
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta content="" name="description">
		<meta content="" name="author">
		<link href="" rel="shortcut icon">
		<title>
			Course Profile
		</title>
		<link href="bootstrap.css" rel="stylesheet">
	</head>
	<body>
		<img border="0" src="banner.jpg" width="100%" height="150">
		<link href="bootstrap.css" rel="stylesheet">
		<ul class="nav nav-pills">
			<li><a href="main.php">Home</a></li>
			<li><a href="info.php">Info</a></li>
			<li><a href="add.php">Add</a></li>
			<li><a href="report.php">Report</a></li>
			<li><a href="course.php">Course</a></li>
			<li><a href="section.php">Section</a></li>
			<li><a href="faculty.php">Faculty</a></li>
			<li><a href="partner.php">Partner</a></li>
			<li><a href="project.php">Project</a></li>
			<li class="active"><a href="admin.php">Administrative</a></li>
			<?php 
			if (phpCAS::isAuthenticated())
			{
				echo '<li><a>You are logged in as <font color="red">' . $casuser . '</font></li></a>';
				echo '<li><a href="?logout">(Logout)</li></a>';
			}
			else echo '<li><a href="login.php">Login</li></a>';
			?>
			<li>
				<form action="keyword.php" method=POST>
					<input type=text align="center" style="width: 25em" name="keyword" placeholder="Search...">
					<input type="submit" name="ksearch" value="Search">
					<br>
					<font color="white">
					<input type="checkbox" name="searchTables[]" value="section" checked>Section
					<input type="checkbox" name="searchTables[]" value="faculty" checked>Faculty
					<input type="checkbox" name="searchTables[]" value="project" checked>Projects
					<input type="checkbox" name="searchTables[]" value="partner" checked>Partners
					<input type="checkbox" name="includeComments" value="Yes" checked>Include Comments
					</font>
				</form>
			</li>
		</ul>
	<p> Warning: Actions in this section will create permanent changes in the entire database!</p>	
		<?php
		
		//Connect to the SLC database
		$database = connectSLC();
		
		//Get the date and time
		$time = time();
		$format = "n/j/y g:ia";
		$dateFormat = new DateTime(date($format, $time));
		$date = $dateFormat->format($format);
		
		
		//if an sql statement is posted, execute it.
		$stmt = $_POST["stmt"];
		$action = $_POST["action"];
		$msg = $_POST["msg"];
		
        //special action, reset the entire database
		if ($action == "reset") 
		{
		    $stmt = file_get_contents('reset.sql');
		}
		
		//special action, Delete by status
		if ($action == "statusDelete") 
		{
		    $stmt = "DELETE FROM project WHERE projectStatus = '{$_POST["projectStatus"]}';";
		}
		
		//execute appropriate statement
		if ($stmt) 
		{
		    $database->fullQuery($stmt);
		    if ($msg) 
		    {
		        echo($msg);
		    }
		}
	?>	
		<div class="container">
			Jump to:
			<a href="#delete">Delete</a> |
			<a href="#export">Export</a> |
			<a href="#import">Import</a> 
			
			<!-------------------------->
			<!--Delete-->
			<!-------------------------->
			
			<a name="delete"></a>
			<h4 align="center"><u><i>Delete</i></u></h4>
			

            <p> Delete all projects by status.</p>
            <form onsubmit="return confirm('WARNING: This will permanently delete values from the Database. Do you want to continue?');" action="admin.php" method=post>
				Status
				<select name="projectStatus">
					<option value="NULL">[None]</option>
						<?php
						//gets all unique statuses as options
							$stmt = $database->prepare("SELECT DISTINCT projectStatus FROM project ORDER BY projectStatus");
							$stmt->execute();
							$result = $stmt->get_result();
							while ($row = mysqli_fetch_array($result))
							{
								$stat = $row["projectStatus"];
								if ($stat != "")
								{
									echo "<option value=\"{$stat}\">{$stat}</option>";
								}
							}
						?>
				</select>

				<br>
				<input type="text" name="action" value="statusDelete" hidden="true" style="display: none">
                <input type="submit" value="Delete">
			</form>

			<!-------------------------->
			<!--Export-->
			<!-------------------------->
	
			<hr style='background:#000000; border:0; height:3px' />
			<a name="export"></a>
			<h4 align="center"><u><i>Export</i></u></h4>
				
			<button> Export Database </button>
			<br>

			<!-------------------------->
			<!--Import-->
			<!-------------------------->

			<hr style='background:#000000; border:0; height:3px' />
			<a name="import"></a>
			<h4 align="center"><u><i>Import</i></u></h4>


            <p>Before importing from a previous backup, the database must be reset. <font color="red">The button below will delete all entries in the database.</font></p>

            <form onsubmit="return confirm('WARNING: This will delete all Database data values. Do you want to continue?');" action="admin.php" method=post>
            <input type="text" name="action" value="reset" hidden="true" style="display: none">
            <input type="submit" value="Reset Database">
            </form> 

            
            <p>Use this button to restore the database to a backup made previously using the export function. Note that you can only restore complete copies of the database. Make sure you empty the database first!</p>
	        <form action="import.php" method="post" enctype="multipart/form-data">
		        <input type="file" name="csvfile" />
		        <input type="submit" value="Send" />	
	        </form>
			<br>

			<br>
			<form action="exportall.php" method="post">
			    <input type="submit" value="Export Database" />
			</form>
		</div>	

	</body>
</html>


